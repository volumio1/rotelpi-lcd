#!/usr/bin/env python2

import requests
import time

from subprocess import check_output
from RPLCD.i2c import CharLCD

lcd = CharLCD(i2c_expander='PCF8574',
              address=0x27,
              port=1,
              cols=20, rows=4, dotsize=8,
              charmap='A02',
              auto_linebreaks=True,
              backlight_enabled=True)

def DisplayInfo():
        """ LCD display info"""
        lcd.cursor_pos = (0, 14)
        lcd.write_string("Vol:")
        lcd.cursor_pos = (0, 18)
        lcd.write_string(str(volume))
        lcd.cursor_pos = (0, 0)
        lcd.write_string(tracktype)
        lcd.cursor_pos = (1, 0)
        lcd.write_string(artiste)
        lcd.cursor_pos = (2, 0)
        lcd.write_string(title)

def DisplayBoot():
        """LCD display boot info"""
        lcd.cursor_pos = (0, 0)
        lcd.write_string("____________________")
        lcd.cursor_pos = (1, 3)
        lcd.write_string("RotelPi")
        lcd.cursor_pos = (2, 3)
        lcd.write_string("Je BOOT :)")
        lcd.cursor_pos = (3, 0)
        lcd.write_string(check_output(['hostname', '-I']))
        time.sleep(10)
        lcd.clear()

lcd.clear()
ip = check_output(['hostname', '-I'])
while not ip:
        DisplayBoot()

titleref = []
title = []
volume =[]
volumeref = []

while ip:
    ## Get info in api http://raspberry ip adress:3000/api/v1/getState
    titleref = title
    volumeref = volume

    response = requests.get('http://rotelpi.local:3000/api/v1/getState')
    info = response.json()
    title = info.get("title")
    tracktype = info.get("trackType")
    artiste = info.get("artist")
    volume = info.get("volume")

    ## Check if info is changed if yes reload LCD
    time.sleep(0.4)
    if titleref != title or volumeref != volume:
        DisplayInfo()
    else:
         pass
